package Triarc;
use strict;
use warnings;
use Math::Bezier;
use Storable;

our $VERSION = 0.01;

=head1 NAME

Triarc - Fitting three arcs to cubic Bezier splines

=head1 SYNOPSIS

Models a Bezier spline with four 2D points and fits three radii as near as
possible to the curve using simulated annealing. The arcs are tangent to the
curve at the ends.  Doesn't deal very well with S shaped splines, very
sensitive to initial arc estimates.

=head1 DESCRIPTION

Create a new Triarc object with a four point 2D Bezier spline:

  $triarc = new Triarc ([0.0,0.0], [1.0,1.0], [2.0,0.0], [2.0,-2.0]);

=cut

sub new
{
    my $class = shift;
    $class = ref $class || $class;

    my $self = {coor_1 => shift, coor_2 => shift, coor_3 => shift, coor_4 => shift};

    bless $self, $class;
    $self->init_bezier;
    $self->guess;
    return $self;
}

sub clone
{
    my $self = shift;
    Storable::dclone ($self);
}

sub permute
{
    my $self = shift;
    my $heat = shift;
    my $clone = $self->clone;
    $clone->{rad_a} = abs ($clone->{rad_a} * (1 + rand (2 * $heat) - $heat));
    $clone->{rad_b} = abs ($clone->{rad_b} * (1 + rand (2 * $heat) - $heat));
    $clone->{rad_c} = abs ($clone->{rad_c} * (1 + rand (2 * $heat) - $heat));
    return $clone;
}

sub init_bezier
{
    my $self = shift;
    $self->{bezier} = new Math::Bezier (@{$self->{coor_1}}, @{$self->{coor_2}},
                                        @{$self->{coor_3}}, @{$self->{coor_4}});
}

sub guess
{
    my $self = shift;
    my @a = _points_2circle ($self->point (0.0), $self->point (0.1), $self->point (0.2));
    my @b = _points_2circle ($self->point (0.4), $self->point (0.5), $self->point (0.6));
    my @c = _points_2circle ($self->point (0.8), $self->point (0.9), $self->point (1.0));

    $self->{rad_a} = $a[1];
    $self->{rad_b} = $b[1];
    $self->{rad_c} = $c[1];
}

sub point
{
    my $self = shift;
    my $point = shift;
    return [$self->{bezier}->point ($point)];
}

=pod

Try and fit some circles to the curve:

  $start_temperature = 0.03;
  $max_iterations = 7000;
  $triarc->anneal ($start_temperature, $max_iterations);

=cut

sub anneal
{
    my $self = shift;
    my ($start_temp, $max_iterations) = @_;
    $start_temp = 0.5 unless defined $start_temp;
    $max_iterations = 10000 unless defined $max_iterations;

    my $initial_error = $self->error || 999999999;
    my $best_error = $initial_error;

    for my $iteration (1 .. $max_iterations)
    {
        my $heat = $start_temp - ($start_temp/$max_iterations * $iteration);
        my $clone = $self->permute ($heat);
        my $error = $clone->error || next;
        print STDERR join ' ', 'Temperature:', sprintf ("%.5f", $heat),
                               'Initial:', sprintf ("%.3f", $initial_error),
                               'Best:', sprintf ("%.3f", $best_error),
                               'Current:', sprintf ("%.3f", $error), "\r";
        next unless $error < $best_error;
        $self->{rad_a} = $clone->{rad_a};
        $self->{rad_b} = $clone->{rad_b};
        $self->{rad_c} = $clone->{rad_c};
        $best_error = $error;
    }
}

sub error
{
    my $self = shift;
    my $coor_a = $self->coor_a;
    my $coor_b = $self->coor_b || return undef;
    my $coor_b_alt = $self->coor_b_alt;
    my $coor_c = $self->coor_c;
    my $max_error = 0.0;
    for my $i (1 .. 99)
    {
        my $p = $self->point ($i/100);
        my $local_error;

        my $error_a = abs (_points_distance ($p, $coor_a) - $self->{rad_a});
        my $error_b = abs (_points_distance ($p, $coor_b) - $self->{rad_b});
        my $error_b_alt = abs (_points_distance ($p, $coor_b_alt) - $self->{rad_b});
        my $error_c = abs (_points_distance ($p, $coor_c) - $self->{rad_c});

        $error_b = $error_b_alt if $error_b > $error_b_alt;

        $local_error = $error_a if ($error_a <= $error_b and $error_a <= $error_c);
        $local_error = $error_b if ($error_b <= $error_a and $error_b <= $error_c);
        $local_error = $error_c if ($error_c <= $error_b and $error_c <= $error_a);

        $max_error = $local_error if $local_error > $max_error;
    }
    return $max_error;
}

=pod

Access the radii of the three circles:

  $rad_a = $triarc->{rad_a};
  $rad_b = $triarc->{rad_b};
  $rad_c = $triarc->{rad_c};

Access the centrepoints of the three circles:

  $coor_a = $triarc->coor_a;
  $coor_b = $triarc->coor_b;
  $coor_c = $triarc->coor_c;

The centrepoint of 'b' has two solutions, you need to check both:

  $coor_b_alt = $triarc->coor_b_alt;

(this is a bug, it shouldn't be hard to fix)

=cut

sub coor_a
{
    my $self = shift;
    my $baseline = _points_2line ($self->{coor_1}, $self->{coor_2});
    my $perpline = _perpendicular_line ($baseline, $self->{coor_3});
    my $intersect = _line_intersection ($baseline, $perpline);
    my $vector = _subtract_vector ($self->{coor_3}, $intersect);
    $vector = _normalise_vector ($vector);
    $vector = _scale_vector ($vector, $self->{rad_a});
    _add_vector ($self->{coor_1}, $vector);
}

sub coor_b
{
    my $self = shift;
    my $rA = abs ($self->{rad_b} - $self->{rad_a});
    my $rB = abs ($self->{rad_b} - $self->{rad_c});
    _circle_intersect_m ($self->coor_a, $self->coor_c, $rA, $rB);
}

sub coor_b_alt
{
    my $self = shift;
    my $rA = abs ($self->{rad_b} - $self->{rad_a});
    my $rB = abs ($self->{rad_b} - $self->{rad_c});
    _circle_intersect_n ($self->coor_a, $self->coor_c, $rA, $rB);
}

sub coor_c
{
    my $self = shift;
    my $baseline = _points_2line ($self->{coor_3}, $self->{coor_4});
    my $perpline = _perpendicular_line ($baseline, $self->{coor_2});
    my $intersect = _line_intersection ($baseline, $perpline);
    my $vector = _subtract_vector ($self->{coor_2}, $intersect);
    $vector = _normalise_vector ($vector);
    $vector = _scale_vector ($vector, $self->{rad_c});
    _add_vector ($self->{coor_4}, $vector);
}

# http://2000clicks.com/mathhelp/GeometryConicSectionCircleIntersection.aspx

sub _circle_intersect_m
{
    my ($A, $B, $rA, $rB) = @_;
    my $d = _points_distance ($A, $B);
    return undef if $d > ($rA + $rB);
    return undef if $d < abs ($rA - $rB);
    return undef if $d == 0;

    my $d2 = ($B->[0] - $A->[0])**2 + ($B->[1] - $A->[1])**2;
    my $K = 0.25 * sqrt ((($rA+$rB)**2 - $d2) * ($d2 - ($rA-$rB)**2));
    my $x = (($B->[0]+$A->[0])/2)
          + (($B->[0]-$A->[0])*($rA**2-$rB**2)/(2*$d2)) + (2*($B->[1]-$A->[1])*$K/$d2);
    my $y = (($B->[1]+$A->[1])/2)
          + (($B->[1]-$A->[1])*($rA**2-$rB**2)/(2*$d2)) + (-2*($B->[0]-$A->[0])*$K/$d2);

    return [$x,$y];
}

sub _circle_intersect_n
{
    my ($A, $B, $rA, $rB) = @_;
    my $d = _points_distance ($A, $B);
    return undef if $d > ($rA + $rB);
    return undef if $d < abs ($rA - $rB);
    return undef if $d == 0;

    my $d2 = ($B->[0] - $A->[0])**2 + ($B->[1] - $A->[1])**2;
    my $K = 0.25 * sqrt ((($rA+$rB)**2 - $d2) * ($d2 - ($rA-$rB)**2));

    my $x = (($B->[0]+$A->[0])/2)
          + (($B->[0]-$A->[0])*($rA**2-$rB**2)/(2*$d2)) - (2*($B->[1]-$A->[1])*$K/$d2);
    my $y = (($B->[1]+$A->[1])/2)
          + (($B->[1]-$A->[1])*($rA**2-$rB**2)/(2*$d2)) - (-2*($B->[0]-$A->[0])*$K/$d2);

    return [$x,$y];
}

sub _perp_bisect
{
    my ($a, $b) = @_;
    my $d = _points_distance ($a, $b);
    my $m = _circle_intersect_m ($a, $b, $d, $d);
    my $n = _circle_intersect_n ($a, $b, $d, $d);
    _points_2line ($m, $n);
}

sub _points_2circle
{
    my ($a, $b, $c) = @_;
    my $line_0 = _perp_bisect ($a, $b);
    my $line_1 = _perp_bisect ($b, $c);
    my $centre = _line_intersection ($line_0, $line_1);
    my $radius = _points_distance ($a, $centre);
    return ($centre, $radius);
}

sub _normalise_vector
{
    my $vector = shift;
    my $length = sqrt ($vector->[0]**2 + $vector->[1]**2);
    _scale_vector ($vector, 1/$length);
    
}

sub _scale_vector
{
    my ($vector, $scale) = @_;
    [$vector->[0] * $scale, $vector->[1] * $scale];
}

sub _subtract_vector
{
    my ($vector_a, $vector_b) = @_;
    [$vector_a->[0] - $vector_b->[0], $vector_a->[1] - $vector_b->[1]];
}

sub _add_vector
{
    my ($vector_a, $vector_b) = @_;
    [$vector_a->[0] + $vector_b->[0], $vector_a->[1] + $vector_b->[1]];
}


sub _points_distance
{
    sqrt (_points_distance2 (@_));
}

sub _points_distance2
{
    my ($a, $b) = @_;
    ($b->[0] - $a->[0])**2 + ($b->[1] - $a->[1])**2;
}

sub _points_2line
{
    my ($coor_0, $coor_1) = @_;
    my $vector = _subtract_vector ($coor_1, $coor_0);
    $vector->[0] = 0.00000000001 unless $vector->[0];
    my $a = $vector->[1] / $vector->[0];
    my $b = $coor_0->[1] - ($coor_0->[0] * $a);
    {a => $a, b => $b};
}

sub _perpendicular_line
{
    my ($line, $coor) = @_;
    $line->{a} = 0.00000000001 unless $line->{a};
    my $a = -1/$line->{a};
    my $b = $coor->[1] - ($coor->[0] * $a);
    {a => $a, b => $b};
}

sub _line_intersection
{
    my ($line_0, $line_1) = @_;
    return undef if ($line_0->{a} == $line_1->{a});
    my $x = ($line_1->{b} - $line_0->{b}) / ($line_0->{a} - $line_1->{a});
    my $y = ($line_0->{a} * $x) + $line_0->{b};
    [$x, $y];
}

=head1 COPYING

Copyright 2011 Bruno Postle <bruno@postle.net>

Free use of this software is granted under the terms of the GNU General
Public License version 2 or any later version.

=cut

1;

