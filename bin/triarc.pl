#!/usr/bin/perl
use strict;
use warnings;

use lib qw|lib ../lib|;
use Triarc;

# fit three arcs to a cubic bezier spline using simulated annealing

my $triarc = new Triarc ([-1018,1529], [-863,1001], [-913,268], [-1487,-733]);

print STDERR join ' ', $triarc->{rad_a}, $triarc->{rad_b}, $triarc->{rad_c} ."\n";

my $start_temp = 0.05;
my $max_iterations = 1000;

unless (defined $triarc->error)
{
    $start_temp *= 4;
    $max_iterations *= 4;
}

$triarc->anneal ($start_temp, $max_iterations);

print STDERR join ' ', "\n". $triarc->{rad_a}, $triarc->{rad_b}, $triarc->{rad_c} ."\n";

0;

