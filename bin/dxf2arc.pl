#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use File::DXF::Util qw /read_DXF dxf/;
use File::DXF;
use Triarc;

die "Fits three circles to Bezier spline(s).
  Spline is defined by a four-point AutoCAD '2D Polyline' with 'Cubic Fit/Smooth'.
  Usage: $0 input.dxf output.dxf" unless @ARGV == 2;

my $data = read_DXF ($ARGV[0], 'CP1252');

open DXF, ">". $ARGV[1];
binmode DXF, ":crlf";

print DXF dxf (0, 'SECTION');
print DXF dxf (2, 'ENTITIES');

my $dxf = new File::DXF;
   $dxf->Process ($data);
my $entities = $dxf->{ENTITIES};

my @report = $entities->Report (('POLYLINE', 'VERTEX', 'SEQEND'));

my $state = 'INIT';
my $vertex_index;
my $index = 0;
my @points;

for my $item (@report)
{
    if ($item->Type eq 'POLYLINE')
    {
        $vertex_index = 0;
        next unless defined $item->{mode};
        next unless $item->{mode} == 4;
        $state = 'POINTS';
        @points = ();
    }

    if ($state eq 'POINTS' and $item->Type eq 'VERTEX')
    {
        next unless defined $item->{mode};
        next unless $item->{mode} == 16;
        print STDERR "node ". ($vertex_index + 1) .": $item->{x}, $item->{y}\n";
        push @points, [$item->{x}, $item->{y}];
        $vertex_index++;
    }

    if ($item->Type eq 'SEQEND' and $state eq 'POINTS')
    {
        my $triarc = new Triarc (@points);
        my $start_temp = 0.03;
        my $max_iterations = 7000;

        $triarc->anneal ($start_temp, $max_iterations);
        print STDERR "\n";

        my $x = $triarc->coor_a->[0];
        my $y = $triarc->coor_a->[1];
        my $r = $triarc->{rad_a};

        print DXF dxf (0, 'CIRCLE');
        print DXF dxf (5, $index);
        print DXF dxf (8, 0);
        print DXF dxf (62, 1);
        print DXF dxf (10, $x);
        print DXF dxf (20, $y);
        print DXF dxf (30, '0.000');
        print DXF dxf (40, $r);

        $index++;

        if (defined $triarc->coor_b)
        {
            $x = $triarc->coor_b->[0];
            $y = $triarc->coor_b->[1];
            $r = $triarc->{rad_b};

            print DXF dxf (0, 'CIRCLE');
            print DXF dxf (5, $index);
            print DXF dxf (8, 0);
            print DXF dxf (62, 3);
            print DXF dxf (10, $x);
            print DXF dxf (20, $y);
            print DXF dxf (30, '0.000');
            print DXF dxf (40, $r);

            $index++;

            $x = $triarc->coor_b_alt->[0];
            $y = $triarc->coor_b_alt->[1];
            $r = $triarc->{rad_b};

            print DXF dxf (0, 'CIRCLE');
            print DXF dxf (5, $index);
            print DXF dxf (8, 0);
            print DXF dxf (62, 3);
            print DXF dxf (10, $x);
            print DXF dxf (20, $y);
            print DXF dxf (30, '0.000');
            print DXF dxf (40, $r);

            $index++;
        }

        $x = $triarc->coor_c->[0];
        $y = $triarc->coor_c->[1];
        $r = $triarc->{rad_c};

        print DXF dxf (0, 'CIRCLE');
        print DXF dxf (5, $index);
        print DXF dxf (8, 0);
        print DXF dxf (62, 4);
        print DXF dxf (10, $x);
        print DXF dxf (20, $y);
        print DXF dxf (30, '0.000');
        print DXF dxf (40, $r);

        $index++;

        $state = 'NEXT';
    }
}

print DXF dxf (0, 'ENDSEC');
print DXF dxf (0, 'EOF');

close DXF;

exit 0;

