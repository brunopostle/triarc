#!/usr/bin/perl

#Editor vim:syn=perl

use strict;
use warnings;
use Test::More 'no_plan';
use lib ('lib', '../lib');

use_ok ('Triarc');

is_deeply (Triarc::_normalise_vector ([3,4]), [0.6,0.8]);
is_deeply (Triarc::_normalise_vector ([-3,4]), [-0.6,0.8]);
is_deeply (Triarc::_normalise_vector ([3,-4]), [0.6,-0.8]);
is_deeply (Triarc::_normalise_vector ([-3,-4]), [-0.6,-0.8]);

is_deeply (Triarc::_scale_vector ([3,4], 2), [6,8]);
is_deeply (Triarc::_scale_vector ([-3,4], 2), [-6,8]);
is_deeply (Triarc::_scale_vector ([3,4], 0.5), [1.5,2]);
is_deeply (Triarc::_scale_vector ([3,4], -1), [-3,-4]);
is_deeply (Triarc::_scale_vector ([-3,-4], -1), [3,4]);

is_deeply (Triarc::_subtract_vector ([2,2], [2,2]), [0,0]);
is_deeply (Triarc::_subtract_vector ([-2,2], [2,-2]), [-4,4]);
is_deeply (Triarc::_subtract_vector ([2,2], [0,0]), [2,2]);
is_deeply (Triarc::_subtract_vector ([0,0], [-2,-2]), [2,2]);

is_deeply (Triarc::_add_vector ([2,2], [2,2]), [4,4]);
is_deeply (Triarc::_add_vector ([-2,2], [2,-2]), [0,0]);
is_deeply (Triarc::_add_vector ([2,2], [0,0]), [2,2]);
is_deeply (Triarc::_add_vector ([0,0], [-2,-2]), [-2,-2]);

is (Triarc::_points_distance ([0,0], [3,4]), 5);
is (Triarc::_points_distance ([3,4], [0,0]), 5);
is (Triarc::_points_distance ([2,0], [4,0]), 2);
is (Triarc::_points_distance ([-4,2], [4,2]), 8);
is (Triarc::_points_distance ([-3,2], [-3,-4]), 6);

is_deeply (Triarc::_points_2line ([0,0], [1,1]), {a=>1,b=>0});
is_deeply (Triarc::_points_2line ([0,0], [3,3]), {a=>1,b=>0});
is_deeply (Triarc::_points_2line ([0,0], [2,0]), {a=>0,b=>0});
is_deeply (Triarc::_points_2line ([0,0], [-2,0]), {a=>0,b=>0});
is_deeply (Triarc::_points_2line ([0,0], [-1,-1]), {a=>1,b=>0});
is_deeply (Triarc::_points_2line ([0,0], [-3,3]), {a=>-1,b=>0});
is_deeply (Triarc::_points_2line ([0,0], [5,4]), {a=>0.8,b=>0});
is_deeply (Triarc::_points_2line ([-1,-1], [2,2]), {a=>1,b=>0});
is_deeply (Triarc::_points_2line ([-1,0], [1,1]), {a=>0.5,b=>0.5});
is_deeply (Triarc::_points_2line ([-3,-1], [1,1]), {a=>0.5,b=>0.5});
is_deeply (Triarc::_points_2line ([-3,-5], [1,-3]), {a=>0.5,b=>-3.5});
is_deeply (Triarc::_points_2line ([-3,-5], [2,-10]), {a=>-1,b=>-8});

is_deeply (Triarc::_perpendicular_line ({a=>-1,b=>-8},[6,-4]), {a=>1,b=>-10});
is_deeply (Triarc::_perpendicular_line ({a=>2,b=>1},[6,3]), {a=>-0.5,b=>6});
is_deeply (Triarc::_perpendicular_line ({a=>2,b=>1},[0,6]), {a=>-0.5,b=>6});
is_deeply (Triarc::_perpendicular_line ({a=>2,b=>1},[-2,7]), {a=>-0.5,b=>6});
is_deeply (Triarc::_perpendicular_line ({a=>2,b=>1},[12,0]), {a=>-0.5,b=>6});
is_deeply (Triarc::_perpendicular_line ({a=>2,b=>1},[14,-1]), {a=>-0.5,b=>6});

is_deeply (Triarc::_line_intersection ({a=>2,b=>1}, {a=>-0.5,b=>6}), [2,5]);
is_deeply (Triarc::_line_intersection ({a=>2,b=>0}, {a=>-0.5,b=>0}), [0,0]);
is_deeply (Triarc::_line_intersection ({a=>2,b=>0}, {a=>0,b=>3}), [1.5,3]);
ok (Triarc::_line_intersection ({a=>2,b=>0}, {a=>0,b=>3}));
ok (!Triarc::_line_intersection ({a=>2,b=>1}, {a=>2,b=>2}));

ok (!Triarc::_circle_intersect_m ([1,1],[1,1.0],3,2));
ok (!Triarc::_circle_intersect_m ([9,1],[9,1.5],3,2));

like (Triarc::_circle_intersect_m ([17,1],[17,2.0],3,2)->[1], '/^4/');
like (Triarc::_circle_intersect_m ([25,1],[25,2.5],3,2)->[1], '/^3\.41/');
like (Triarc::_circle_intersect_m ([33,1],[33,5.5],3,2)->[1], '/^3\.80/');
like (Triarc::_circle_intersect_m ([41,1],[41,6.0],3,2)->[1], '/^4/');

ok (!Triarc::_circle_intersect_n ([49,1],[49,6.5],3,2));

ok (!Triarc::_circle_intersect_n ([1,1],[1,1.0],3,2));
ok (!Triarc::_circle_intersect_n ([9,1],[9,1.5],3,2));

like (Triarc::_circle_intersect_n ([17,1],[17,2.0],3,2)->[1], '/^4/');
like (Triarc::_circle_intersect_n ([25,1],[25,2.5],3,2)->[1], '/^3\.41/');
like (Triarc::_circle_intersect_n ([33,1],[33,5.5],3,2)->[1], '/^3\.80/');
like (Triarc::_circle_intersect_n ([41,1],[41,6.0],3,2)->[1], '/^4/');

ok (!Triarc::_circle_intersect_n ([49,1],[49,6.5],3,2));

is_deeply ([Triarc::_points_2circle ([5,7],[-2,6],[2,-2])], [[2,3], 5]);

