#!/usr/bin/perl

#Editor vim:syn=perl

use strict;
use warnings;
use Test::More 'no_plan';
use lib ('lib', '../lib');

use_ok ('Triarc');

my $triarc = new Triarc ([2,5], [8,13], [12,13], [21,1]);

$triarc->{rad_a} = 5;
$triarc->{rad_b} = 25;
$triarc->{rad_c} = 10;

is_deeply ($triarc->coor_a, [6,2]);

#is_deeply ($triarc->coor_c, [13,-5]);
is ($triarc->coor_c->[0], 13);
like ($triarc->coor_c->[1], '/^-4.999999/');

like ($triarc->coor_b->[0], '/^5.506/');
like ($triarc->coor_b->[1], '/^-17.993/');

$triarc->init_bezier;

is_deeply ($triarc->point (0.1), [3.747,7.156]);
is_deeply ($triarc->point (0.2), [5.416,8.808]);
is_deeply ($triarc->point (0.3), [7.049,9.932]);
is_deeply ($triarc->point (0.4), [8.688,10.504]);
is_deeply ($triarc->point (0.5), [10.375,10.5]);

like ($triarc->error, '/^3\.90689/');

my $clone = $triarc->clone;

like ($clone->error, '/^3\.90689/');

$triarc->{coor_2} = [200,200];
$triarc->init_bezier;

like ($clone->error, '/^3\.90689/');

ok ($triarc->error > 80);

$clone->{rad_a} = 25;
$clone->{rad_b} = 6;
$clone->{rad_c} = 48;

#use Data::Dumper; die Dumper $clone->coor_b;
